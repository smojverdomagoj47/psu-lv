import matplotlib.pyplot as plt
import numpy as np
import skimage.io

b_blok = 255* np.ones((50,50))
c_blok = np.zeros((50,50))

x= input ("Veličina x šahovnice: ")
y= input ("Veličina y šahovnice: ")

xtemp = float(x)/2
ytemp = float(y)/2    
xtemp2 = xtemp%1
ytemp2 = ytemp%1
if xtemp2 < 0.5:
    xtemp = xtemp-xtemp2
else:    
    xtemp = +1
    xtemp2 = xtemp%1
    xtemp = xtemp-xtemp2
if ytemp2 < 0.5:   
    ytemp = ytemp-ytemp2 
else:
    ytemp = +1
    ytemp2 = ytemp-ytemp2
    ytemp = ytemp-ytemp2
img = np.hstack((c_blok, b_blok)*int(xtemp))
img2 = np.hstack((b_blok, c_blok)*int(xtemp))
img3 = np.vstack((img,img2)*int(ytemp))
plt.figure(1)
plt.imshow(img3, cmap='gray', vmin=0, vmax=255)
plt.show()