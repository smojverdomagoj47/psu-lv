import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(open("mtcars.csv", "rb"), usecols=(1,2,3,4,5,6), delimiter=",", skiprows=1)


mpg = data[:, 0]
hp = data[:, 3]
wt = data[:, 5]


print(data)

summpg = mpg.sum
print("Minimalna vrijednost potrosnje MPG: ", mpg.min)
print("Maksimalna vrijednost potrosnje MPG: ", mpg.max)
print("Srednja vrijednost potrosnje MPG: ", summpg/mpg.len)

plt.scatter(mpg, hp, s=wt*10)

plt.xlabel("MPG")
plt.ylabel("HP")

plt.show()
