import matplotlib.pyplot as plt
import numpy as np
import skimage.io


img = skimage.io.imread('tiger.png', as_gray=True)
h,w=img.shape

img_copy = img.copy()


#a)
#for i in range(h):
#    for j in range(w):
#        if img_copy[i,j]<205:
#            img_copy[i,j]+=50    
#        else:
#            img_copy[i,j]=255  


#b)
#img_copy = np.zeros((w,h))
#for i in range(h):
#    for j in range(w):
#        img_copy[j,h-i-1]=img[i,j]


#c)
#img_copy = np.zeros((h,w))
#for i in range(h):
#    for j in range(w):
#        img_copy[i,w-j-1]=img[i,j]


#d)
img_copy = img[0:-1:10,0:-1:10]

plt.figure(1)
plt.imshow(img, cmap='gray', vmin=0, vmax=255)

plt.figure(2)
plt.imshow(img_copy, cmap='gray', vmin=0, vmax=255)
plt.show()
