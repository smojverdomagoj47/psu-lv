import numpy as np
import matplotlib.pyplot as plt

vrijednosti = np.empty(1, int)

for i in range (100):
    vrijednosti = np.append(vrijednosti, np.random.randint(1,7))



print(vrijednosti)

plt.xlabel('Kockica')
plt.ylabel('Broj ponavljanja')
plt.title('Histogram bacanja kockice')
plt.hist(vrijednosti, bins=10, align = 'left')
plt.axis([0, 8, 0, 30])
plt.show()