import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, max_error

# ucitavanje ociscenih podataka
df = pd.read_csv('cars_processed.csv')
print(df.info())

# izbacivanje nepotrebnih stupaca
df = df.drop(['name', 'mileage'], axis=1)

# odabir ulaznih velicina
X = df[['km_driven', 'year', 'engine', 'max_power']]
y = df['selling_price']

# podjela na train i test skup podataka
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=10)
print()

# skaliranje ulaznih velicina
Scaler = MinMaxScaler()
X_train_s = Scaler.fit_transform(X_train)
X_test_s = Scaler.transform(X_test)

# izrada modela
linear_model = LinearRegression()
linear_model.fit(X_train_s, y_train)

# evaluacija modela
y_pred_train = linear_model.predict(X_train_s)
y_pred_test = linear_model.predict(X_test_s)

print("R2 test: ", r2_score(y_pred_test, y_test))
print("RMSE test: ", np.sqrt(mean_squared_error(y_pred_test, y_test)))
print("Max error test: ", max_error(y_pred_test, y_test))
print("MAE test: ", mean_absolute_error(y_pred_test, y_test))