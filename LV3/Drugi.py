import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 1.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars.groupby('cyl').mpg.mean().plot.bar()
#plt.show()

# 2.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars.boxplot(column = 'wt', by='cyl')
#plt.show()

# 3.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars.boxplot(column = "mpg", by = "am")
#plt.show()

# 4.
mtcars = pd.read_csv('mtcars.csv')
mtcars[(mtcars.am==0)].plot.scatter(x = "qsec", y = "hp", c = 'b', marker = 'x', label = 'automatski')
mtcars[(mtcars.am==1)].plot.scatter(x = "qsec", y = "hp", c = 'r', marker = 's', label = 'manual')
#mtcars_new2 = mtcars[('am'==1)].plot.scatter(x = "qsec", y = "hp", color = "DarkGreen", mtcars_new1 = mtcars_new1)
plt.show()
