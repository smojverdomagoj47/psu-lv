import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 1.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars.sort_values(by=['mpg'])
#print(mtcars_new.head(5))

# 2.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[mtcars.cyl == 8]
#mtcars_new = mtcars_new.sort_values(by=['mpg'], ascending = False)
#print(mtcars_new.head(3))

# 3.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[mtcars.cyl == 6]
#print(mtcars_new.mean().mpg)

# 4.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.0) & (mtcars.wt<=2.2)]
#print(mtcars_new.mean().mpg)

# 5.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[(mtcars.am == 1)].am
#print("Broj automobila s manualnim mijenjacem: ", mtcars_new.sum())
#sum = mtcars_new.sum()
#print("Broj automobila s automatskim mijenjacem: ", len(mtcars)-sum)

# 6.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[(mtcars.am == 0) & (mtcars.hp>100)].am
#print("Broj automobila s automatskim mijenjacem: ", len(mtcars_new))

#7.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[['car', 'wt']]
#mtcars_new["wt"] = 2205 * mtcars_new["wt"]
#print(mtcars_new)