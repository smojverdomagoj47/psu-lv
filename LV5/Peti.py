# -*- coding: utf-8 -*-
"""
Created on Sun Dec 02 12:08:00 2018

@author: Grbic
"""
import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt

slika = mpimg.imread('example.png')
    
X = slika.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=5, n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
slika_compressed = np.choose(labels, values)
slika_compressed.shape = slika.shape

plt.figure(1)
plt.imshow(slika,  cmap='gray')
plt.show()

plt.figure(2)
plt.imshow(slika_compressed,  cmap='gray')
plt.show()