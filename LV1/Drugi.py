try:
    broj = float(input("Unesi broj koji predstavlja ocjenu:"))
except:
    print("Greska!")
    exit()

if (broj<0.0 or broj>1.0):
    print("Broj nije u odgovarajucem intervalu!")
    exit()

if (broj>=0.9):
    print("Ocjena: A")

elif (broj>=0.8):
    print ("Ocjena: B")

elif (broj>=0.7):
    print ("Ocjena: C")

elif (broj>=0.6):
    print ("Ocjena: D")

else:
    print ("Ocjena: F" )