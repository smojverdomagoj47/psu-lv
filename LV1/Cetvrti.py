brojevi = []
num = 0
sum = 0

while True:
    temp = input("Unesi broj: ")
    if temp=="Done":
        break
    if temp.isnumeric():
        brojevi.append(temp)
        num=num+1
        sum=sum+int(temp)
        
    else:
        print("Krivi unos! Pokusaj ponovno!")
    
print("Broj brojeva: ", num)
print("Srednja vrijednost: ", sum/num)
print("Najmanja vrijednost: ", min(brojevi))
print("Najveca vrijednost: ", max(brojevi))    

    
    