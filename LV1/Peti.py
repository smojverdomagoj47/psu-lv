numbers = []

while True:
    temp = input("Datoteka? ")
    if temp == "mbox.txt" or "mbox-short.txt":
        break
    else:
        print("Krivi unos!")

file1 = open(temp, "r")
for line in file1:
    line = line.rstrip()
    if line.startswith('X-DSPAM-Confidence:'):
        value = line.split(':')[-1]
        numbers.append(float(value))
print(numbers)
print("Ime datoteke: ", temp)
print("Srednja vrijednost pouzdanosti: ", sum(numbers)/len(numbers))